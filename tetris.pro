#-------------------------------------------------
#
# Project created by QtCreator 2016-01-10T12:56:13
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = tetris
TEMPLATE = app

CONFIG += c++11

SOURCES += main.cpp\
        widget.cpp \
    tetrisboard.cpp \
    tetrispiece.cpp

HEADERS  += widget.h \
    tetrisboard.h \
    tetrispiece.h

FORMS    += widget.ui
