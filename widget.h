#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include "tetrispiece.h"

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

private slots:
    void onLevelChanged(int level);

private:
    Ui::Widget *ui;
};

#endif // WIDGET_H
