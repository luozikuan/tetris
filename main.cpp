#include "widget.h"
#include <QApplication>
#include <ctime>
#include <QSettings>

int main(int argc, char *argv[])
{
    QCoreApplication::setOrganizationName("luozikuan");
    QCoreApplication::setOrganizationDomain("luozikuan.com");
    QCoreApplication::setApplicationName("tetris");

    qsrand((uint)time(NULL));
    QApplication a(argc, argv);
    Widget w;
    w.show();

    return a.exec();
}
