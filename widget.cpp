#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    ui->tetrisBoard->setFocus();
    ui->tetrisBoard->setNextPieceLabel(ui->labelNextPiece);
    connect(ui->tetrisBoard, SIGNAL(levelChanged(int)), SLOT(onLevelChanged(int)));
}

Widget::~Widget()
{
    delete ui;
}

void Widget::onLevelChanged(int level)
{
    ui->lcdLevel->display(level + 1);
}
