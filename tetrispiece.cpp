#include "tetrispiece.h"

TetrisPiece::TetrisPiece(TetrisShape shape)
{
    setShape(shape);
}

TetrisShape TetrisPiece::shape() const
{
    return m_shape;
}

void TetrisPiece::setShape(TetrisShape shape)
{
    static const int coordsTable[8][4][2] = {
        { { 0, 0 },   { 0, 0 },   { 0, 0 },   { 0, 0 } },
        { { 0, -1 },  { 0, 0 },   { -1, 0 },  { -1, 1 } },
        { { 0, -1 },  { 0, 0 },   { 1, 0 },   { 1, 1 } },
        { { 0, -1 },  { 0, 0 },   { 0, 1 },   { 0, 2 } },
        { { -1, 0 },  { 0, 0 },   { 1, 0 },   { 0, 1 } },
        { { 0, 0 },   { 1, 0 },   { 0, 1 },   { 1, 1 } },
        { { -1, -1 }, { 0, -1 },  { 0, 0 },   { 0, 1 } },
        { { 1, -1 },  { 0, -1 },  { 0, 0 },   { 0, 1 } }
    };

    for (int i = 0; i < 4; i++) {
        coords[i].setX(coordsTable[shape][i][0]);
        coords[i].setY(coordsTable[shape][i][1]);
    }
    m_shape = shape;
}

void TetrisPiece::setRandomShape()
{
    setShape(TetrisShape(1 + qrand() % (TOTAL_SHAPE - 1)));
}

QPoint &TetrisPiece::index(int i)
{
    return coords[i];
}

TetrisPiece TetrisPiece::rotateLeft()
{
    if (m_shape == SquareShape) {
        return *this;
    }

    TetrisPiece result;
    result.m_shape = m_shape;
    for (int i = 0; i < 4; i++) {
        result.index(i).setX(this->index(i).y());
        result.index(i).setY(-this->index(i).x());
    }
    return result;
}

TetrisPiece TetrisPiece::rotateRight()
{
    if (m_shape == SquareShape) {
        return *this;
    }

    TetrisPiece result;
    result.m_shape = m_shape;
    for (int i = 0; i < 4; i++) {
        result.index(i).setX(-this->index(i).y());
        result.index(i).setY(this->index(i).x());
    }
    return result;
}


int TetrisPiece::minX()
{
    int min = this->index(0).x();
    for (int i = 1; i < 4; i++) {
        int x = this->index(i).x();
        if (x < min)
            min = x;
    }
    return min;
}

int TetrisPiece::minY()
{
    int min = this->index(0).y();
    for (int i = 1; i < 4; i++) {
        int y = this->index(i).y();
        if (y < min)
            min = y;
    }
    return min;
}

int TetrisPiece::maxX()
{
    int max = this->index(0).x();
    for (int i = 1; i < 4; i++) {
        int x = this->index(i).x();
        if (x > max)
            max = x;
    }
    return max;
}

int TetrisPiece::maxY()
{
    int max = this->index(0).y();
    for (int i = 1; i < 4; i++) {
        int y = this->index(i).y();
        if (y > max)
            max = y;
    }
    return max;
}
