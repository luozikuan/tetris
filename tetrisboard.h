#ifndef TETRISBOARD_H
#define TETRISBOARD_H

#include <QFrame>
#include <QTime>
#include <QTimer>
#include <QPointer>
#include <QLabel>
#include <QPaintEvent>
#include <QSettings>
#include "tetrispiece.h"

class TetrisBoard : public QFrame
{
    Q_OBJECT
public:
    explicit TetrisBoard(QWidget *parent = 0);
    virtual ~TetrisBoard();
    QSizeF squareSize();
    void setNextPieceLabel(QPointer<QLabel> label);
    QSettings &settings();

signals:
    void levelChanged(int level);
    void scoreChanged(int score);
    void pauseChanged(bool paused);
    void nextPieceChanged();

public slots:
    void restart();
    void pauseSwitch();
    void resetSettings();
    void resetKeymap();

protected:
    void paintEvent(QPaintEvent * event);
    QSize minimumSizeHint() const;
    void keyPressEvent(QKeyEvent * event);

private slots:
    void oneLineDown();
    void onNextPieceChanged();

private:
    enum {
        boardWidth = 10,
        boardHeight = 22,
    };

    void clearBoard();
    void generateNextPiece();
    TetrisPiece &shapeAt(Coordinate pos);
    TetrisPiece &shapeAt(int x, int y);
    void drawSquare(QPainter &painter, QPointF topLeft, TetrisPiece piece);
    void drawSquare(QPainter &painter, QPointF topLeft, QColor color);
    void drawBgLine(QPainter &painter);
    void drawShadowPiece(QPainter &painter);
    int getDropSpeed();
    void putPieceToBoard();
    void checkFullLines();
    bool tryMove(TetrisPiece newPiece, Coordinate newPos);
    void dropDown();
    void setScore(int s);
    void saveBoard();
    void readSettings();

    TetrisPiece board[boardWidth][boardHeight];
    TetrisPiece curPiece, nextPiece;
    QPointer<QLabel> labelNextPiece;
    bool paused;
    bool started;
    Coordinate curPos;
    int linePieceCnt[boardHeight];
    QSettings m_settings;

    int level; // to control drop speed
    int score;
    QTimer dropTimer;
};

#endif // TETRISBOARD_H
