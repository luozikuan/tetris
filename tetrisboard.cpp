#include <QPainter>
#include <QDebug>
#include <QRgb>
#include <QFileInfo>
#include "tetrisboard.h"

TetrisBoard::TetrisBoard(QWidget *parent)
    : QFrame(parent)
    , curPiece(NoShape)
    , nextPiece(NoShape)
    , paused(false)
    , started(false)
    , level(0)
    , score(0)
{
    this->setFocusPolicy(Qt::StrongFocus);
    connect(&dropTimer, SIGNAL(timeout()), SLOT(oneLineDown()));
    connect(this, SIGNAL(nextPieceChanged()), SLOT(onNextPieceChanged()));

    readSettings();
}

TetrisBoard::~TetrisBoard()
{

}

TetrisPiece &TetrisBoard::shapeAt(Coordinate pos)
{
    return board[pos.x()][pos.y()];
}

TetrisPiece &TetrisBoard::shapeAt(int x, int y)
{
    return board[x][y];
}

void TetrisBoard::clearBoard()
{
    int i, j;
    for (j = 0; j < boardHeight; j++) {
        for (i = 0; i < boardWidth; i++) {
            shapeAt(i, j).setShape(NoShape);
        }
        linePieceCnt[j] = 0;
    }
}

void TetrisBoard::setNextPieceLabel(QPointer<QLabel> label)
{
    labelNextPiece = label;
}

void TetrisBoard::generateNextPiece()
{
    curPiece = nextPiece;
    curPos = Coordinate(boardWidth/2, boardHeight-1-curPiece.maxY());
    if (!tryMove(curPiece, curPos)) {
        // cannot put new piece, game over
        dropTimer.stop();
        qDebug() << "game over";
        started = false;
        return ;
    }

    nextPiece.setRandomShape();
    emit nextPieceChanged();
    dropTimer.setInterval(getDropSpeed());
    dropTimer.start();
}

void TetrisBoard::onNextPieceChanged()
{
    if (!labelNextPiece)
        return;

    QPixmap pixmap(labelNextPiece->contentsRect().size());
    QPainter painter(&pixmap);
    painter.fillRect(pixmap.rect(), labelNextPiece->palette().background());
    QPointF offset;
    offset.setX((nextPiece.maxX() + nextPiece.minX()) / 2.0 * squareSize().width());
    offset.setY((nextPiece.maxY() + nextPiece.minY()) / 2.0 * squareSize().height());
    painter.translate(pixmap.rect().center() - offset);

    for (int i = 0; i < 4; ++i) {
        int x = nextPiece.index(i).x();
        int y = nextPiece.index(i).y();
        drawSquare(painter,
                   QPointF(x * squareSize().width(), -y * squareSize().height()),
                   nextPiece);
    }
    labelNextPiece->setPixmap(pixmap);
}

int TetrisBoard::getDropSpeed()
{
    // TODO: to implement a better mapper
    return 300 - level * 25;
}

void TetrisBoard::oneLineDown()
{
    if (!tryMove(curPiece, curPos + Coordinate(0, -1)))
    {
        // reach to bottom, check if have full line
        dropTimer.stop();
        putPieceToBoard();
        checkFullLines();
        generateNextPiece();
    }
}

void TetrisBoard::dropDown()
{
    while (tryMove(curPiece, curPos + Coordinate(0, -1))) ;
    // reach to bottom, check if have full line
    dropTimer.stop();
    putPieceToBoard();
    checkFullLines();
    generateNextPiece();
}

bool TetrisBoard::tryMove(TetrisPiece newPiece, Coordinate newPos)
{
    for (int i = 0; i < 4; i++) {
        Coordinate pos = newPos + newPiece.index(i);
        if (pos.x() < 0 || pos.x() >= boardWidth
                || pos.y() < 0 || pos.y() >= boardHeight
                || shapeAt(pos).shape() != NoShape) {
            return false;
        }
    }
    curPos = newPos;
    curPiece = newPiece;
    update();
    return true;
}

void TetrisBoard::putPieceToBoard()
{
    for (int i = 0; i < 4; i++) {
        Coordinate pos = curPos + curPiece.index(i);
        shapeAt(pos).setShape(curPiece.shape());
        linePieceCnt[pos.y()]++;
    }
}

void TetrisBoard::checkFullLines()
{
    int fullLines = 0;
    for (int j = 0; j < boardHeight;) {
        if (linePieceCnt[j] == boardWidth) {
            fullLines++;
            // full, remove this line
            bool noNeedToFillTopLine = false;
            for (int line = j; line < boardHeight - 1; line++) {
                if (linePieceCnt[line] == 0) {
                    noNeedToFillTopLine = true;
                    break;
                }
                for (int w = 0; w < boardWidth; w++) {
                    shapeAt(w, line).setShape(shapeAt(w, line+1).shape());
                }
                linePieceCnt[line] = linePieceCnt[line + 1];
            }
            if (noNeedToFillTopLine) {
                continue;
            }

            for (int w = 0; w < boardWidth; w++) {
                shapeAt(w, boardHeight - 1).setShape(NoShape);
            }
            linePieceCnt[boardHeight - 1] = 0;
        } else if (linePieceCnt[j] == 0) {
            break;
        } else {
            j++;
        }
    }

    if (fullLines == 0) {
        return ;
    } else if (fullLines == 1) {
        setScore(score + 1);
    } else if (fullLines == 2) {
        setScore(score + 3);
    } else if (fullLines == 3) {
        setScore(score + 6);
    } else if (fullLines == 4) {
        setScore(score + 10);
    }
    emit scoreChanged(score);
}

void TetrisBoard::setScore(int s)
{
    score = s;
    int newLevel = s / 10;
    if (newLevel != level) {
        level = newLevel;
        emit levelChanged(level);
    }
}

void TetrisBoard::paintEvent(QPaintEvent * event)
{
    QFrame::paintEvent(event);

    QPainter painter(this);
    painter.translate(QPoint(1, 1)); // QFrame have border
    if (paused) {
        painter.drawText(contentsRect(), Qt::AlignCenter, tr("Paused"));
    } else {
        QSizeF squareSize = this->squareSize();

        if (m_settings.value("backgroundLine").toBool())
            drawBgLine(painter);

        int i, j;
        for (j = 0; j < boardHeight; j++) {
            for (i = 0; i < boardWidth; i++) {
                drawSquare(painter,
                           QPointF(i * squareSize.width(), (boardHeight - 1 - j) * squareSize.height()),
                           shapeAt(i, j));
            }
        }

        if (started) {
            if (m_settings.value("shadowPiece").toBool())
                drawShadowPiece(painter);

            for (i = 0; i < 4; i++) {
                Coordinate pos = curPos + curPiece.index(i);
                drawSquare(painter,
                           QPointF(pos.x() * squareSize.width(), (boardHeight - 1 - pos.y()) * squareSize.height()),
                           curPiece);
            }
        }
    }
}

QSizeF TetrisBoard::squareSize()
{
    return QSizeF(contentsRect().width() / (double)boardWidth,
                 contentsRect().height() / (double)boardHeight);
}

QSize TetrisBoard::minimumSizeHint() const
{
    return QSize(20 * boardWidth, 20 * boardHeight);
}

void TetrisBoard::drawSquare(QPainter &painter, QPointF topLeft, TetrisPiece piece)
{
    if (piece.shape() == NoShape)
        return ;

    static const QRgb colorTable[TOTAL_SHAPE] = {
        0xefefef,
        /*0xea4328,*/ 0xfc7802, 0xfbd500, 0xa2cb5b,
        0x5bd4d0, 0x3195e9, 0x876fbd, 0xf5a0a0,
    };

    QColor color = colorTable[(int)piece.shape()];
    //QColor color = colorTable[rand() % (TOTAL_SHAPE-1)+1];
    drawSquare(painter, topLeft, color);

}

void TetrisBoard::drawSquare(QPainter &painter, QPointF topLeft, QColor color)
{
    QRectF squareRect = QRectF(topLeft, this->squareSize() - QSizeF(1, 1));
    painter.fillRect(squareRect, QBrush(color));

    painter.setPen(color.light());
    painter.drawLine(squareRect.topLeft(), squareRect.topRight());
    painter.drawLine(squareRect.topLeft(), squareRect.bottomLeft());

    painter.setPen(color.dark());
    painter.drawLine(squareRect.bottomLeft(), squareRect.bottomRight());
    painter.drawLine(squareRect.topRight(), squareRect.bottomRight());
}

void TetrisBoard::drawBgLine(QPainter &painter)
{
    int value = 220;
    QColor color(value, value, value);
    painter.save();
    painter.setPen(color);
    int i, j;
    for (i = 1; i < boardWidth; i++) {
        painter.drawLine(QPointF(i * squareSize().width(), 0),
                         QPointF(i * squareSize().width(), contentsRect().height()));
    }
    for (j = 1; j < boardHeight; j++) {
        painter.drawLine(QPointF(0, j * squareSize().height()),
                         QPointF(contentsRect().width(), j * squareSize().height()));
    }
    painter.restore();
}

void TetrisBoard::drawShadowPiece(QPainter &painter)
{
    Coordinate shadowPos = curPos;
    bool stop = false;
    forever {
        shadowPos += Coordinate(0, -1);
        for (int i = 0; i < 4; i++) {
            Coordinate pos = shadowPos + curPiece.index(i);
            if (shapeAt(pos).shape() != NoShape
                    || pos.y() < 0) {
                stop = true;
                break;
            }
        }
        if (stop)
            break;
    }
    shadowPos += Coordinate(0, 1);
    QSizeF squareSize = this->squareSize();
    for (int i = 0; i < 4; i++) {
        Coordinate pos = shadowPos + curPiece.index(i);
        drawSquare(painter,
                   QPointF(pos.x() * squareSize.width(), (boardHeight  - 1 - pos.y()) * squareSize.height()),
                   Qt::lightGray);
    }
}

void TetrisBoard::resetKeymap()
{
    m_settings.setValue("keymap/MoveLeft",    (int)Qt::Key_Left);
    m_settings.setValue("keymap/MoveRight",   (int)Qt::Key_Right);
    m_settings.setValue("keymap/RotateLeft",  (int)Qt::Key_Up);
    m_settings.setValue("keymap/RotateRight", (int)Qt::Key_Down);
    m_settings.setValue("keymap/DropOneLine", (int)Qt::Key_D);
    m_settings.setValue("keymap/DropDown",    (int)Qt::Key_Space);
    m_settings.setValue("keymap/Restart",     (int)Qt::Key_Return);
    m_settings.setValue("keymap/Pause",       (int)Qt::Key_P);
}

void TetrisBoard::keyPressEvent(QKeyEvent *event)
{
    if (!started) {
        if (event->key() == (Qt::Key)m_settings.value("keymap/Restart").toInt()) {
            restart();
        }
        goto END_COPE;
    }
    if (paused && event->key() != (Qt::Key)m_settings.value("keymap/Pause").toInt()) {
        goto END_COPE;
    }

    if (event->key() == (Qt::Key)m_settings.value("keymap/MoveLeft").toInt())
        tryMove(curPiece, curPos + Coordinate(-1, 0));
    else if (event->key() == (Qt::Key)m_settings.value("keymap/MoveRight").toInt())
        tryMove(curPiece, curPos + Coordinate(1, 0));
    else if (event->key() == (Qt::Key)m_settings.value("keymap/RotateLeft").toInt())
        tryMove(curPiece.rotateRight(), curPos);
    else if (event->key() == (Qt::Key)m_settings.value("keymap/RotateRight").toInt())
        tryMove(curPiece.rotateLeft(), curPos);
    else if (event->key() == (Qt::Key)m_settings.value("keymap/DropDown").toInt())
        dropDown();
    else if (event->key() == (Qt::Key)m_settings.value("keymap/DropOneLine").toInt())
        oneLineDown();
    else if (event->key() == (Qt::Key)m_settings.value("keymap/Restart").toInt())
        restart();
    else if (event->key() == (Qt::Key)m_settings.value("keymap/Pause").toInt())
        pauseSwitch();

END_COPE:
    QFrame::keyPressEvent(event);
}

void TetrisBoard::pauseSwitch()
{
    if (!started)
        return ;

    paused = !paused;
    if (paused) {
        dropTimer.stop();
    } else {
        dropTimer.start();
    }
    update();
    emit pauseChanged(paused);
}

QSettings &TetrisBoard::settings()
{
    return m_settings;
}

void TetrisBoard::readSettings()
{
    if (!QFileInfo::exists(m_settings.fileName())) {
        resetSettings();
        m_settings.sync();
    }
}

void TetrisBoard::resetSettings()
{
    m_settings.setValue("shadowPiece", false);
    m_settings.setValue("backgroundLine", true);

    resetKeymap();
}

void TetrisBoard::restart()
{
    started = true;
    level = 0;
    emit levelChanged(level);
    score = 0;
    emit scoreChanged(score);

    clearBoard();

    nextPiece.setRandomShape();
    emit nextPieceChanged();
    generateNextPiece();
}
