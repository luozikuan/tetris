#ifndef TETRISSHAPE_H
#define TETRISSHAPE_H

#include <QPoint>

typedef QPoint Coordinate;

enum TetrisShape {
    NoShape,
    ZShape,
    SShape,
    LineShape,
    TShape,
    SquareShape,
    LShape,
    MirroredLShape,

    TOTAL_SHAPE,
};

class TetrisPiece
{
public:
    explicit TetrisPiece(TetrisShape shape = NoShape);
    TetrisShape shape() const;
    void setShape(TetrisShape shape);
    void setRandomShape();
    Coordinate &index(int i);
    TetrisPiece rotateLeft();
    TetrisPiece rotateRight();
    int minX();
    int minY();
    int maxX();
    int maxY();

private:
    TetrisShape m_shape;
    Coordinate coords[4];
};

#endif // TETRISSHAPE_H
